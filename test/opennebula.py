from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver
from libcloud.compute.base import NodeImage, NodeSize
import time, urlparse

class OpenNebulaVNet(object):
    def __init__(self, vnet_id):
        self.id = vnet_id
        self.address = None
    def __str__(self):
        return str(self.id)

def opennebula_context():
        cloud_script = 'if [ -f /mnt/context.sh ]; then\n' \
                + '. /mnt/context.sh\n' \
                + 'fi\n' \
                + '/sbin/ifconfig eth0 $IP_PUBLIC\n' \
                + '/sbin/ip route add default via $IP_GATEWAY\n' \
                + 'echo "nameserver $NAMESERVER" > /etc/resolv.conf\n' \
                + 'echo "prepend domain-name-servers $NAMESERVER;" >> ' \
                + '/etc/dhcp/dhclient.conf\n'
        return cloud_script

def get_context():
        final_script = opennebula_context()
        context_vars = {}
        #context_vars['HOSTNAME'] = 'lulu'
        #context_vars['IP_PUBLIC'] = '$NIC[IP]'
        #context_vars['IP_GATEWAY'] = '10.100.255.254'
        #context_vars['NAMESERVER'] = '10.100.255.254'
        #context_vars['USERDATA'] = final_script.encode("hex")
        context_vars['HOSTNAME'] = "$NAME"
    	context_vars['DNS'] = "$NETWORK[DNS,     NETWORK_ID=1]"
    	context_vars['GATEWAY'] = "$NETWORK[GATEWAY, NETWORK_ID=1]"
    	context_vars['NETMASK'] = "$NETWORK[NETMASK, NETWORK_ID=1]"
    	context_vars['IP_PRIVATE'] = "$NIC[IP, NETWORK_ID=1]"
	context_vars['USERDATA'] = '23212f62696e2f626173680a200a6966205b202d66202f6d6e742f636f6e' \
		'746578742e7368205d3b207468656e0a20202e202f6d6e742f636f6e7465' \
		'78742e73680a66690a0a686f73746e616d652024484f53544e414d450a69' \
		'66636f6e6669672065746830202449505f50524956415445206e65746d61' \
		'736b20244e45544d41534b200a726f757465206164642064656661756c74' \
		'2067772024474154455741592065746830200a0a6563686f20226e616d65' \
		'7365727665722024444e5322203e202f6574632f7265736f6c762e636f6e' \
		'660a200a2320546f206d616b652073736820776f726b20696e2044656269' \
		'616e204c656e6e793a0a236170742d67657420696e7374616c6c20756465' \
		'760a236563686f20226e6f6e65202f6465762f7074732064657670747320' \
		'64656661756c74732030203022203e3e202f6574632f66737461620a236d' \
		'6f756e74202d610a'
	context_vars['TARGET'] = 'hdc'
        return context_vars

driver = get_driver(Provider.OPENNEBULA)
url = urlparse.urlparse('http://fs0.das4.cs.vu.nl:4567')
conn = driver('aion', secret = 'JkUb2cE6',
	secure = (url.scheme == 'https'), host = url.hostname,
        port = url.port, api_version = '2.2')
instance_types = [i for i in conn.list_sizes()]

print instance_types

image = NodeImage(id = '764', name = '', driver = None)
size = instance_types[1]
network = OpenNebulaVNet('1')
#context = self.get_context(script)
context = get_context()
instance = conn.create_node(name = 'compnode3', image = image,
	size = size, networks = network, os_arch = 'i686',
        os_root = 'hda', disk_target = 'hda', context = context)


#import pdb
#pdb.set_trace()

#time.sleep(20)

#instance.destroy()
