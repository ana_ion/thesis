from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

import libcloud.security
import time
from pprint import pprint

libcloud.security.VERIFY_SSL_CERT = False

OpenStack = get_driver(Provider.OPENSTACK)
driver = OpenStack('admin', 'password',
        		   host='10.100.0.42', port=35357, secure=False,
                   ex_force_auth_url='http://10.100.0.42:35357',
                   ex_force_auth_version='2.0_password',
		           ex_force_service_name='nova',
		           ex_tenant_name = 'admin')

sizes = driver.list_sizes()
print sizes

no_tests = 10
print "Running %d tests..." % no_tests

time.sleep(3)

count = 0
while count < no_tests:
    target = ''
    source = ''
    if count % 2 == 0:
        source = 'compnode3'
        target = 'compnode2'
    else:
        source = 'compnode2'
        target = 'compnode3'
    
    start_time = time.time()
    driver.migrate_vm('e206ff07-c080-40ba-931c-e6eadcfa2d62', target, True)

    while True:
        nodes = driver.list_nodes()
        conpaas_vm = [node for node in nodes if node.name == 'conpaas2'][0]
        hostname = conpaas_vm.extra['hostName']
        if hostname == target:
            break
        time.sleep(1)

    end_time = time.time()

#    print "migrated from %s to %s in:" %(source, target)
    print end_time - start_time
    
    count += 1

    time.sleep(10)

